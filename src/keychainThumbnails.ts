import { S3 } from 'aws-sdk';
import { removeImage, uploadImage } from './s3';
import { compressImage, readLinkIDsFromFile } from './util';

const getKeychainImageKeys = async (
  s3: S3,
  bucket: string
): Promise<string[]> => {
  const keychainImageKeys: string[] = [];

  let keychainImages = await s3
    .listObjectsV2({
      Bucket: bucket,
      Prefix: 'public/keychain/',
    })
    .promise();

  keychainImages.Contents?.forEach(
    (keychainImage) =>
      !!keychainImage.Key && keychainImageKeys.push(keychainImage.Key)
  );

  while (keychainImages.NextContinuationToken) {
    const continuationToken = keychainImages.NextContinuationToken;
    keychainImages = await s3
      .listObjectsV2({
        Bucket: bucket,
        Prefix: 'public/keychain/',
        ContinuationToken: continuationToken,
      })
      .promise();
    keychainImages.Contents?.forEach(
      (keychainImage) =>
        !!keychainImage.Key && keychainImageKeys.push(keychainImage.Key)
    );
  }

  const filterkeychainContentImageKeys = keychainImageKeys?.filter(
    (keychainImageKey) =>
      !keychainImageKey?.match(/public\/keychain\/.{0,}\/.{0,}-content\/.{0,}/)
  );

  const filterkeychainPreviewImageKeys = filterkeychainContentImageKeys?.filter(
    (keychainImageKey) =>
      !keychainImageKey?.match(
        /public\/keychain\/.{0,}\/.{0,}\/.{0,}-previewFile.{0,}/
      )
  );

  return filterkeychainPreviewImageKeys;
};

const getWebsiteLinkThumbnailKeys = (
  keychainImageKeys: string[],
  env: string
): string[] => {
  const websiteLinkIDs = readLinkIDsFromFile(`db/website-links_${env}.txt`);

  return keychainImageKeys.filter((keychainImageKey) => {
    let match = false;

    websiteLinkIDs.forEach((websiteLinkID) => {
      if (
        keychainImageKey.match(
          new RegExp(
            `public\/keychain\/.{0,}\/.{0,}-${websiteLinkID}[\/|.].{0,}`,
            'g'
          )
        )
      ) {
        match = true;
        return;
      }
    });

    return match;
  });
};

const getKeyLinkThumbnailKeys = (
  keychainImageKeys: string[],
  env: string
): string[] => {
  const keyLinkIDs = readLinkIDsFromFile(`db/key-links_${env}.txt`);

  return keychainImageKeys.filter((keychainImageKey) => {
    let match = false;

    keyLinkIDs.forEach((keyLinkID) => {
      if (
        keychainImageKey.match(
          new RegExp(
            `public\/keychain\/.{0,}\/.{0,}-${keyLinkID}[\/|.].{0,}`,
            'g'
          )
        )
      ) {
        match = true;
        return;
      }
    });

    return match;
  });
};

const uploadWeblinkThumbnail = async (
  s3: S3,
  bucket: string,
  key: string,
  image: Buffer
) => {
  const compressedImage = await compressImage(image, 80, 80, 'PNG');

  await uploadImage(s3, bucket, key.replace('jpg', 'png'), compressedImage);

  await removeImage(s3, bucket, key.replace('png', 'jpg'));
};

const uploadKeylinkThumbnail = async (
  s3: S3,
  bucket: string,
  key: string,
  image: Buffer
) => {
  const compressedImage = await compressImage(image, 200, 200);

  await uploadImage(s3, bucket, key.replace('png', 'jpg'), compressedImage);

  await removeImage(s3, bucket, key.replace('jpg', 'png'));
};

const processWebsiteLinkThumbnail = async (
  s3: S3,
  bucket: string,
  key: string
) => {
  try {
    const websiteLinkThumbnail = await s3
      .getObject({
        Bucket: bucket,
        Key: key,
      })
      .promise();

    await uploadWeblinkThumbnail(
      s3,
      bucket,
      key,
      websiteLinkThumbnail.Body as Buffer
    );
  } catch (error) {
    console.log(error);
  }
};

const processKeyLinkThumbnail = async (s3: S3, bucket: string, key: string) => {
  try {
    const keyLinkThumbnail = await s3
      .getObject({
        Bucket: bucket,
        Key: key,
      })
      .promise();

    await uploadKeylinkThumbnail(
      s3,
      bucket,
      key,
      keyLinkThumbnail.Body as Buffer
    );
  } catch (error) {
    console.log(error);
  }
};

const logKeys = (message: string, keys: string[]) => {
  console.log(`\n-----\n${message}:\n`);
  keys.forEach((key) => console.log(key));
};

export const compressKeychainThumbnails = async (
  s3: S3,
  bucket: string,
  env: string
) => {
  let keychainImageKeys = await getKeychainImageKeys(s3, bucket);

  let websiteLinkThumbnailKeys = getWebsiteLinkThumbnailKeys(
    keychainImageKeys,
    env
  );

  logKeys('Website link keys before processing', websiteLinkThumbnailKeys);

  console.log('\n-----\nRequests and responses:\n');

  await Promise.all(
    websiteLinkThumbnailKeys.map((websiteLinkThumbnailKey) =>
      processWebsiteLinkThumbnail(s3, bucket, websiteLinkThumbnailKey)
    )
  );

  let keyLinkThumbnailKeys = getKeyLinkThumbnailKeys(keychainImageKeys, env);

  logKeys('Key link keys before processing', keyLinkThumbnailKeys);

  console.log('\n-----\nRequests and responses:\n');

  await Promise.all(
    keyLinkThumbnailKeys.map((keyLinkThumbnailKey) =>
      processKeyLinkThumbnail(s3, bucket, keyLinkThumbnailKey)
    )
  );

  // after processing

  keychainImageKeys = await getKeychainImageKeys(s3, bucket);

  websiteLinkThumbnailKeys = getWebsiteLinkThumbnailKeys(
    keychainImageKeys,
    env
  );

  logKeys('Website link keys after processing', websiteLinkThumbnailKeys);

  keyLinkThumbnailKeys = getKeyLinkThumbnailKeys(keychainImageKeys, env);

  logKeys('Key link keys after processing', keyLinkThumbnailKeys);
};
