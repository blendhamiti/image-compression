import { S3 } from 'aws-sdk';

export const uploadImage = async (
  s3: S3,
  bucket: string,
  key: string,
  image: Buffer
) => {
  const res = await s3
    .putObject({
      Bucket: bucket,
      Key: key,
      Body: image,
    })
    .promise();

  console.log({
    request: {
      method: 'PUT',
      bucket,
      key,
      size: image.byteLength,
    },
    response: {
      status: res.$response.httpResponse.statusCode,
      date: res.$response.httpResponse.headers?.['date'],
    },
  });
};

export const removeImage = async (s3: S3, bucket: string, key: string) => {
  const res = await s3
    .deleteObject({
      Bucket: bucket,
      Key: key,
    })
    .promise();

  console.log({
    request: {
      method: 'DELETE',
      bucket,
      key,
    },
    response: {
      status: res.$response.httpResponse.statusCode,
      date: res.$response.httpResponse.headers?.['date'],
    },
  });
};
