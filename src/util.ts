import * as fs from 'fs';
import sharp from 'sharp';

export const compressImage = async (
    image: Buffer,
    height: number,
    width: number,
    fileType: 'JPG' | 'PNG' = 'JPG'
  ) => {
    const resizedImage = sharp(image).resize(width, height);
    if (fileType === 'PNG') {
      return await resizedImage.png().toBuffer();
    }
    return await resizedImage.jpeg().toBuffer();
  };

export const readLinkIDsFromFile = (path: string): number[] => {
  const fileContent = fs.readFileSync(path, {
    encoding: 'utf-8',
  });

  return fileContent
    .split(/\n/g)
    .slice(1)
    .map((value) => Number(value));
}