import { S3 } from 'aws-sdk';
import { compressKeychainThumbnails } from './keychainThumbnails';
import { compressLegacyUserAvatars, compressUserAvatars } from './userAvatars';

const main = async () => {
  const env = process.argv.slice(2)[0];

  let bucket;
  switch (env) {
    case 'dev':
      bucket = 'key-web-appffa82bd654674d1d8f8a3778ef685c58-dev';
      break;

    case 'qa':
      bucket = 'key-web-appffa82bd654674d1d8f8a3778ef685c58-qa';
      break;

    case 'prod':
      bucket = 'key-web-app6c1faeee8f5a4e578f6a8b89124832d5-prod';
      break;
  }

  if (bucket) {
    const s3 = new S3();

    try {
      await compressUserAvatars(s3, bucket);

      await compressLegacyUserAvatars(s3, bucket);

      await compressKeychainThumbnails(s3, bucket, env);
    } catch (error) {
      console.log(error);
    }
  }
};

main();
