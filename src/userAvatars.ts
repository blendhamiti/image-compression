import { S3 } from 'aws-sdk';
import { removeImage, uploadImage } from './s3';
import { compressImage } from './util';

const uploadImages = async (
  s3: S3,
  bucket: string,
  dir: string,
  image: Buffer
) => {
  const compressedImage = await compressImage(image, 200, 200);
  const compressedNavbarImage = await compressImage(image, 60, 60);

  await Promise.all([
    uploadImage(s3, bucket, `${dir}/avatar.jpg`, compressedImage),
    uploadImage(s3, bucket, `${dir}/avatar-small.jpg`, compressedNavbarImage),
  ]);

  await removeImage(s3, bucket, `${dir}/avatar.png`);
};

const getDirs = async (s3: S3, bucket: string): Promise<string[]> => {
  let userAvatarDirs: string[] = [];

  const pathPrefix = 'public/users/';

  let userAvatars = await s3
    .listObjectsV2({
      Bucket: bucket,
      Prefix: pathPrefix,
    })
    .promise();

  const userAvatarsKeys = userAvatars.Contents?.flatMap(
    (userAvatar) => userAvatar.Key
  );

  if (userAvatarsKeys) {
    userAvatarDirs = [
      ...userAvatarDirs,
      ...userAvatarsKeys?.map((value) => {
        const userAvatarDirBreadcrumb = value?.split('/');
        return `${userAvatarDirBreadcrumb?.[0]}/${userAvatarDirBreadcrumb?.[1]}/${userAvatarDirBreadcrumb?.[2]}`;
      }),
    ];
  }

  while (userAvatars.NextContinuationToken) {
    const continuationToken = userAvatars.NextContinuationToken;

    userAvatars = await s3
      .listObjectsV2({
        Bucket: bucket,
        Prefix: pathPrefix,
        ContinuationToken: continuationToken,
      })
      .promise();

    const userAvatarsKeys = userAvatars.Contents?.flatMap(
      (userAvatar) => userAvatar.Key
    );

    if (userAvatarsKeys) {
      userAvatarDirs = [
        ...userAvatarDirs,
        ...userAvatarsKeys?.map((value) => {
          const userAvatarDirBreadcrumb = value?.split('/');
          return `${userAvatarDirBreadcrumb?.[0]}/${userAvatarDirBreadcrumb?.[1]}/${userAvatarDirBreadcrumb?.[2]}`;
        }),
      ];
    }
  }

  return Array.from(new Set(userAvatarDirs));
};

const processDir = async (
  s3: S3,
  bucket: string,
  dir: string
) => {
  try {
    const userAvatars = await s3
      .listObjects({
        Bucket: bucket,
        Prefix: dir,
      })
      .promise();

    const jpgImage = userAvatars.Contents?.find((userAvatar) =>
      userAvatar.Key?.endsWith('avatar.jpg')
    );

    const pngImage = userAvatars.Contents?.find((userAvatar) =>
      userAvatar.Key?.endsWith('avatar.png')
    );

    if ((jpgImage && jpgImage.Key) || (pngImage && pngImage.Key)) {
      const imageKey = (jpgImage?.Key || pngImage?.Key) as string;

      const image = await s3
        .getObject({
          Bucket: bucket,
          Key: imageKey,
        })
        .promise();

      await uploadImages(s3, bucket, dir, image.Body as Buffer);
    }
  } catch (error) {
    console.log(error);
  }
};

const processDirAsLegacy = async (
  s3: S3,
  bucket: string,
  dir: string
) => {
  try {
    const userAvatars = await s3
      .listObjects({
        Bucket: bucket,
        Prefix: dir,
      })
      .promise();

    const jpgImage = userAvatars.Contents?.find((userAvatar) =>
      userAvatar.Key?.endsWith('profile-image.jpg')
    );

    const pngImage = userAvatars.Contents?.find((userAvatar) =>
      userAvatar.Key?.endsWith('profile-image.png')
    );

    if ((jpgImage && jpgImage.Key) || (pngImage && pngImage.Key)) {
      const imageKey = (jpgImage?.Key || pngImage?.Key) as string;

      const image = await s3
        .getObject({
          Bucket: bucket,
          Key: imageKey,
        })
        .promise();

      await uploadImages(s3, bucket, dir, image.Body as Buffer);
    }
  } catch (error) {
    console.log(error);
  }
};

const logImageKeys = async (s3: S3, bucket: string): Promise<string[]> => {
  let userAvatarKeys: string[] = [];

  const pathPrefix = 'public/users/';

  let userAvatars = await s3
    .listObjectsV2({
      Bucket: bucket,
      Prefix: pathPrefix,
    })
    .promise();

  const currIterKeys = userAvatars.Contents?.flatMap(
    (userAvatar) => userAvatar.Key || ''
  );

  if (currIterKeys) {
    userAvatarKeys = [...userAvatarKeys, ...currIterKeys];
  }

  while (userAvatars.NextContinuationToken) {
    const continuationToken = userAvatars.NextContinuationToken;

    userAvatars = await s3
      .listObjectsV2({
        Bucket: bucket,
        Prefix: pathPrefix,
        ContinuationToken: continuationToken,
      })
      .promise();

    const currIterKeys = userAvatars.Contents?.flatMap(
      (userAvatar) => userAvatar.Key || ''
    );

    if (currIterKeys) {
      userAvatarKeys = [...userAvatarKeys, ...currIterKeys];
    }
  }

  userAvatarKeys.forEach((key) => console.log(key));

  return userAvatarKeys;
};

const logLegacyImageKeys = async (s3: S3, bucket: string) => {
  const userAvatarKeys = await logImageKeys(s3, bucket);

  userAvatarKeys
    .filter((key) => key.match(/profile-image/g))
    .forEach((key) => console.log(key));
};

export const compressUserAvatars = async (s3: S3, bucket: string) => {
  const userAvatarDirs = await getDirs(s3, bucket);

  console.log('\n-----\nAvatar images before running compression script:\n');

  await logImageKeys(s3, bucket);

  console.log('\n-----\nRequests and responses:\n');

  await Promise.all(
    userAvatarDirs.map(userAvatarDir => processDir(s3, bucket, userAvatarDir))
  )

  console.log('\n-----\nAvatar images after running compression script:\n');

  await logImageKeys(s3, bucket);
};

export const compressLegacyUserAvatars = async (s3: S3, bucket: string) => {
  const userAvatarDirs = await getDirs(s3, bucket);

  console.log('\n-----\nLegacy Avatar images before running compression script:\n');

  await logLegacyImageKeys(s3, bucket);

  console.log('\n-----\nRequests and responses:\n');

  await Promise.all(
    userAvatarDirs.map(userAvatarDir => processDirAsLegacy(s3, bucket, userAvatarDir))
  )

  console.log('\n-----\nLegacy Avatar images after running compression script:\n');

  await logLegacyImageKeys(s3, bucket);
};
